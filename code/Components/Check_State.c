sbit S1 at RA1_bit;  // State 1
sbit S2 at RA2_bit;  // State2
sbit S3 at RA3_bit;  // State3
sbit AL at RA0_bit;  // Alarm

sbit btn_Modif at RD1_bit;  // modifica la Fecha/Hora seleccionada
sbit btn_Enter at RD2_bit;  // asigna el cambio de modo
sbit btn_Modo  at RD3_bit;  // Cambio de Modo
sbit btn_FH    at RD0_bit;

int e=0;

void Mode_State(){

  do{
   if (btn_Modo == 0)
    {
     delay_ms(25);
     if (btn_Modo == 1){
      e++;
     }
    }
   if (e == 1) {S1 = 1; S3 = 0;}               // Modo 1
   if (e == 2) {S2 = 1; S1 = 0;}               // Modo 2
   if (e == 3) {S3 = 1; S2 = 0;}               // Modo 3
   if (e > 3) {e = 0; S1 = 0; S2 = 0; S3 = 0;} // Vuelve al Modo 0
   }while(btn_enter == 1);
}
void Mode_State();