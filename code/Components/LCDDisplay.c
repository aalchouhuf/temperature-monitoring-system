// LCD module connections
sbit LCD_RS at RB4_bit;
sbit LCD_EN at RB5_bit;
sbit LCD_D4 at RB0_bit;
sbit LCD_D5 at RB1_bit;
sbit LCD_D6 at RB2_bit;
sbit LCD_D7 at RB3_bit;

sbit LCD_RS_Direction at TRISB4_bit;
sbit LCD_EN_Direction at TRISB5_bit;
sbit LCD_D4_Direction at TRISB0_bit;
sbit LCD_D5_Direction at TRISB1_bit;
sbit LCD_D6_Direction at TRISB2_bit;
sbit LCD_D7_Direction at TRISB3_bit;
// End LCD module connections

char copyrigth[] = "All Rigth";
char reserved_to[] = "Reserved to:";
char memb1_name[] = "Ayham Al Chouhuf";
char memb1_CI[] = "V24.554.488";
char memb2_name[] = "Manuel Hurtado";
char memb2_CI[] = "V20.699.300";

char Time[] = "Hora:  :  :  ";
unsigned short Segs, Ms, Hs, Date, DD, MM, AA, i, k = 0;
char Calendario[] = "Fecha:   /  /20  ";
char Temperatura[] = "T=";

void Move_Delay() {                  // Function used for text moving
  Delay_ms(500);                     // You can change the moving speed here
}

void set_bits_for_LCD(){
  ANSEL  = 0;                        // Configure AN pins as digital I/O
  ANSELH = 0;
  C1ON_bit = 0;                      // Disable comparators
  C2ON_bit = 0;
}

void Lcd_wait_500(){
  Delay_ms(500);                     // Delay 500ms
  Lcd_Cmd(_LCD_CLEAR);               // Clear display
}

void Lcd_write_two_lines(char text_row_1[], char text_row_2[]){
  Lcd_Out(1,1,text_row_1);                 // Write text in first row
  Lcd_Out(2,1,text_row_2);                 // Write text in second row
}

void Lcd_show_copyrigth(){
  Lcd_Cmd(_LCD_CLEAR);               // Clear display
  Lcd_Cmd(_LCD_CURSOR_OFF);          // Cursor off
  Lcd_Out(1,6,copyrigth);            // Write text in first row
  Lcd_Out(2,6,reserved_to);                 // Write text in second row
  Delay_ms(2000);
  
  Lcd_Cmd(_LCD_CLEAR);               // Clear display

  Lcd_Out(1,1,memb1_name);                 // Write text in first row
  Lcd_Out(2,5,memb1_CI);                 // Write text in second row
  Delay_ms(2000);

  Lcd_Cmd(_LCD_CLEAR);               // Clear display

  Lcd_Out(1,1,memb2_name);                 // Write text in first row
  Lcd_Out(2,5,memb2_CI);                 // Write text in second row
  Delay_ms(2000);

  Lcd_Cmd(_LCD_CLEAR);               // Clear display

  Lcd_write_two_lines(memb1_name, memb2_name);
}

void DS1337_Display()
{
  Time[12] = Segs%10 + 48;
  Time[11] = Segs/10 + 48;
  Time[9]  = Ms % 10 + 48;
  Time[8]  = Ms / 10 + 48;
  Time[6]  = Hs % 10 + 48;
  Time[5]  = Hs / 10 + 48;
  Calendario[8]  = DD % 10 + 48;
  Calendario[7]  = DD / 10 + 48;
  Calendario[11] = MM % 10 + 48;
  Calendario[10] = MM / 10 + 48;
  Calendario[16] = AA % 10 + 48;
  Calendario[15] = AA / 10 + 48;

  lcd_out(2,1,Calendario);
  lcd_out(1,1, Time);
}

void Blink()
{
 int j = 0;
 while(j < 10 && btn_FH == 1 && Modif == 1)
  {
   j++;
   Delay_ms(25);
  }
}

void Muestra_En_Display(){
  Read_RTC();        // Lee los registros del RTC
  DS1337_Display();  // Se muestra la Fecha y hora ajustadas en la pantalla LCD

  Write_Temp_init();          // Inicializa el sensor de temperatura
  Dato = Read_Full_Temp();    // lectura del sensor
   if (Dato > 20.0)  M0 = 1;  // Si T > 20?C se enciende una alarma
   if (Dato <= 20.0) M0 = 0;

   lcd_out(1,15,Temperatura);
  floattostr_fixlen(dato, txt, 5);
  lcd_out(1,17, txt);
  Delay_ms(200);
}


// Function declarations
void Move_Delay();
void Lcd_show_copyrigth();
void Lcd_wait_500ms();
void Lcd_write_two_lines(char text_row_1[], char text_row_2[]);