void Helper_config_components()
{
  TRISA = 0x00;             // Puerto A como salida
  TRISD = 0xFF;             // Puerto D como entrada
  
  PortA = 0;
  ANSEL = 0;                //
  ANSELH = 0;               // Puerto B digital

  Lcd_Init();               // Initialize LCD
  Lcd_Cmd(_LCD_CLEAR);      // Clear display
  Lcd_Cmd(_LCD_CURSOR_OFF); // Cursor off
}