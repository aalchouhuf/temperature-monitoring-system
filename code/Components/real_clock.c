void Write_RTC()              // Escritura en RTC
{
   Soft_I2C_Init();           // Inicializa la comunicacion I2C
   Soft_I2C_Start();          // Inicializa la transmision I2C
   Soft_I2C_Write(0xD0);      // Palabra de Control para el RTC: Escritura
   Soft_I2C_Write(0);         // direccion a escribir en el RTC
   Soft_I2C_Write(0);         // Escribe los Segundos
   Soft_I2C_Write(Ms);        // Escribe los Minutos
   Soft_I2C_Write(HS);        // Escribe las horas
   Soft_I2C_Write(1);         // Escribe el dia de la semana
   Soft_I2C_Write(DD);        // Escribe el dia
   Soft_I2C_Write(MM);        // Escribe el mes
   Soft_I2C_Write(AA);        // Escribe el a?o
   Soft_I2C_Stop();           // Finalizacion de la transmision
   Delay_ms(500);
}

/*** DS1337C Reloj de tiempo Real: lectura de los registros ***/
void Read_RTC(){
  Soft_I2C_Init();            // initialize I2C communication
  Soft_I2C_Start();           // issue I2C start signal
  Soft_I2C_Write(0xD0);       // Palabra de control: Escritura
  Soft_I2C_Write(0);
  Soft_I2C_Start();
  Soft_I2C_Write(0xD1);            // Palabra de control: Lectura
  Segs= bcd2dec(Soft_I2C_Read(1));
  Ms  = bcd2dec(Soft_I2C_Read(1));
  Hs  = bcd2dec(Soft_I2C_Read(1));
  Date= bcd2dec(Soft_I2C_Read(1));
  DD  = bcd2dec(Soft_I2C_Read(1));
  MM  = bcd2dec(Soft_I2C_Read(1));
  AA  = bcd2dec(Soft_I2C_Read(0));
  Soft_I2C_Stop();           // Finalizacion de la transmision
}