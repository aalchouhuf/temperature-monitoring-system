void Write_Temp_init(){
   Soft_I2C_Init();             // Inicializa la comunicacion I2C
   Soft_I2C_Start();            // Inicializa la transmision I2C
   Soft_I2C_Write(0x90);        // Palabra de Control A0 = A1 = A2 = 0
   Soft_I2C_Write(0xEE);        // Inicio de la conversion
   Soft_I2C_Stop();             // Finalizacion de la transmision
}

float Read_Full_Temp(){
   signed int DataxH;
   int DataxL;
   float Tura;
   Soft_I2C_Init();             // Inicializa la comunicacion I2C
   Soft_I2C_Start();            // Inicializa la transmision I2C
   Soft_I2C_Write(0x90);        // Palabra de Control A0 = A1 = A2 = 0
   Soft_I2C_Write(0xAA);        // comando para lectura de la conversion
   Soft_I2C_Start();
   Soft_I2C_Write(0x91);        // Palabra de Control Modo lectura
   DataxH = Soft_I2C_Read(1);   // Lectura de la parte alta
   DataxL = Soft_I2C_Read(0);   // lectura de la parte baja
   Soft_I2C_Stop();             // Finalizacion de la transmision
   tura = DataxH;              // conversion a flotante
   if (DataxL == 128) tura = tura + 0.5;
   return(tura);
}
