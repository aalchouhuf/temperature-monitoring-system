void Write_EEPROM(long int Addres, unsigned short Data)
{                             // Escritura
 Soft_I2C_Init();
 Soft_I2C_Start();            // Inicializa la transmision I2C
 Soft_I2C_Write(0xA0);        // Palabra de Control
 Soft_I2C_Write(Addres >> 8); // Parte alta de la direccion a escribir en la EEPROM
 Soft_I2C_Write(Addres);      // Parte Baja de la direccion a escribir en la EEPROM
 Soft_I2C_Write(Data);        // Dato a escribir
 Soft_I2C_Stop();             // Finalizacion de la transmision
 delay_ms(200);
}

unsigned int Read_EEPROM(long int Address){
  int Data;                   // Lectura
  Soft_I2C_Init();
  Soft_I2C_Start();            // Inicializa la transmision I2C
  Soft_I2C_Write(0xA0);        // Palabra de Control
  Soft_I2C_Write(Address >> 8);
  Soft_I2C_Write(Address);
  Soft_I2C_Start();
  Soft_I2C_Write(0xA1);        // Palabra de Control
  Data = Soft_I2C_Read(0);
  Soft_I2C_Stop();             // Finalizacion de la transmision
  return (Data);
}

void Write_Float_EEPROM(long int n, float Data){
  int i;
  for (i = 0; i < 4; i++)
  Write_EEPROM(i + n, *((short*)(&Data) + i));
}

float Read_Float_EEPROM(long int n){
 int i;
 float Data;
 for (i = 0; i < 4; i++)
 *((short*)(&Data) + i) =  Read_EEPROM(i + n);
 return (Data);
}

