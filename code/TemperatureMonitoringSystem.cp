#line 1 "F:/Projects/PIC/temperature-monitoring-system/code/TemperatureMonitoringSystem.c"
#line 1 "f:/projects/pic/temperature-monitoring-system/code/../code/components/helper.c"
void Helper_config_components()
{
 TRISA = 0x00;
 TRISD = 0xFF;

 PortA = 0;
 ANSEL = 0;
 ANSELH = 0;

 Lcd_Init();
 Lcd_Cmd(_LCD_CLEAR);
 Lcd_Cmd(_LCD_CURSOR_OFF);
}
#line 1 "f:/projects/pic/temperature-monitoring-system/code/../code/components/lcddisplay.c"

sbit LCD_RS at RB4_bit;
sbit LCD_EN at RB5_bit;
sbit LCD_D4 at RB0_bit;
sbit LCD_D5 at RB1_bit;
sbit LCD_D6 at RB2_bit;
sbit LCD_D7 at RB3_bit;

sbit LCD_RS_Direction at TRISB4_bit;
sbit LCD_EN_Direction at TRISB5_bit;
sbit LCD_D4_Direction at TRISB0_bit;
sbit LCD_D5_Direction at TRISB1_bit;
sbit LCD_D6_Direction at TRISB2_bit;
sbit LCD_D7_Direction at TRISB3_bit;


char copyrigth[] = "All Rigth";
char reserved_to[] = "Reserved to:";
char memb1_name[] = "Ayham Al Chouhuf";
char memb1_CI[] = "V24.554.488";
char memb2_name[] = "Manuel Hurtado";
char memb2_CI[] = "V20.699.300";

char Time[] = "Hora:  :  :  ";
unsigned short Segs, Ms, Hs, Date, DD, MM, AA, i, k = 0;
char Calendario[] = "Fecha:   /  /20  ";
char Temperatura[] = "T=";

void Move_Delay() {
 Delay_ms(500);
}

void set_bits_for_LCD(){
 ANSEL = 0;
 ANSELH = 0;
 C1ON_bit = 0;
 C2ON_bit = 0;
}

void Lcd_wait_500(){
 Delay_ms(500);
 Lcd_Cmd(_LCD_CLEAR);
}

void Lcd_write_two_lines(char text_row_1[], char text_row_2[]){
 Lcd_Out(1,1,text_row_1);
 Lcd_Out(2,1,text_row_2);
}

void Lcd_show_copyrigth(){
 Lcd_Cmd(_LCD_CLEAR);
 Lcd_Cmd(_LCD_CURSOR_OFF);
 Lcd_Out(1,6,copyrigth);
 Lcd_Out(2,6,reserved_to);
 Delay_ms(2000);

 Lcd_Cmd(_LCD_CLEAR);

 Lcd_Out(1,1,memb1_name);
 Lcd_Out(2,5,memb1_CI);
 Delay_ms(2000);

 Lcd_Cmd(_LCD_CLEAR);

 Lcd_Out(1,1,memb2_name);
 Lcd_Out(2,5,memb2_CI);
 Delay_ms(2000);

 Lcd_Cmd(_LCD_CLEAR);

 Lcd_write_two_lines(memb1_name, memb2_name);
}

void DS1337_Display()
{
 Time[12] = Segs%10 + 48;
 Time[11] = Segs/10 + 48;
 Time[9] = Ms % 10 + 48;
 Time[8] = Ms / 10 + 48;
 Time[6] = Hs % 10 + 48;
 Time[5] = Hs / 10 + 48;
 Calendario[8] = DD % 10 + 48;
 Calendario[7] = DD / 10 + 48;
 Calendario[11] = MM % 10 + 48;
 Calendario[10] = MM / 10 + 48;
 Calendario[16] = AA % 10 + 48;
 Calendario[15] = AA / 10 + 48;

 lcd_out(2,1,Calendario);
 lcd_out(1,1, Time);
}

void Blink()
{
 int j = 0;
 while(j < 10 && btn_FH == 1 && Modif == 1)
 {
 j++;
 Delay_ms(25);
 }
}

void Muestra_En_Display(){
 Read_RTC();
 DS1337_Display();

 Write_Temp_init();
 Dato = Read_Full_Temp();
 if (Dato > 20.0) M0 = 1;
 if (Dato <= 20.0) M0 = 0;

 lcd_out(1,15,Temperatura);
 floattostr_fixlen(dato, txt, 5);
 lcd_out(1,17, txt);
 Delay_ms(200);
}



void Move_Delay();
void Lcd_show_copyrigth();
void Lcd_wait_500ms();
void Lcd_write_two_lines(char text_row_1[], char text_row_2[]);
#line 1 "f:/projects/pic/temperature-monitoring-system/code/../code/components/check_state.c"
sbit S1 at RA1_bit;
sbit S2 at RA2_bit;
sbit S3 at RA3_bit;
sbit AL at RA0_bit;

sbit btn_Modif at RD1_bit;
sbit btn_Enter at RD2_bit;
sbit btn_Modo at RD3_bit;
sbit btn_FH at RD0_bit;

int e=0;

void Mode_State(){

 do{
 if (btn_Modo == 0)
 {
 delay_ms(25);
 if (btn_Modo == 1){
 e++;
 }
 }
 if (e == 1) {S1 = 1; S3 = 0;}
 if (e == 2) {S2 = 1; S1 = 0;}
 if (e == 3) {S3 = 1; S2 = 0;}
 if (e > 3) {e = 0; S1 = 0; S2 = 0; S3 = 0;}
 }while(btn_enter == 1);
}
void Mode_State();
#line 1 "f:/projects/pic/temperature-monitoring-system/code/../code/components/i2c_controller.c"

sbit Soft_I2C_Scl at RC3_bit;
sbit Soft_I2C_Sda at RC4_bit;

sbit Soft_I2C_Scl_Direction at TRISC3_bit;
sbit Soft_I2C_Sda_Direction at TRISC4_bit;
#line 1 "f:/projects/pic/temperature-monitoring-system/code/../code/components/eprom_memory.c"
void Write_EEPROM(long int Addres, unsigned short Data)
{
 Soft_I2C_Init();
 Soft_I2C_Start();
 Soft_I2C_Write(0xA0);
 Soft_I2C_Write(Addres >> 8);
 Soft_I2C_Write(Addres);
 Soft_I2C_Write(Data);
 Soft_I2C_Stop();
 delay_ms(200);
}

unsigned int Read_EEPROM(long int Address){
 int Data;
 Soft_I2C_Init();
 Soft_I2C_Start();
 Soft_I2C_Write(0xA0);
 Soft_I2C_Write(Address >> 8);
 Soft_I2C_Write(Address);
 Soft_I2C_Start();
 Soft_I2C_Write(0xA1);
 Data = Soft_I2C_Read(0);
 Soft_I2C_Stop();
 return (Data);
}

void Write_Float_EEPROM(long int n, float Data){
 int i;
 for (i = 0; i < 4; i++)
 Write_EEPROM(i + n, *((short*)(&Data) + i));
}

float Read_Float_EEPROM(long int n){
 int i;
 float Data;
 for (i = 0; i < 4; i++)
 *((short*)(&Data) + i) = Read_EEPROM(i + n);
 return (Data);
}
#line 1 "f:/projects/pic/temperature-monitoring-system/code/../code/components/temperature_sensor.c"
void Write_Temp_init(){
 Soft_I2C_Init();
 Soft_I2C_Start();
 Soft_I2C_Write(0x90);
 Soft_I2C_Write(0xEE);
 Soft_I2C_Stop();
}

float Read_Full_Temp(){
 signed int DataxH;
 int DataxL;
 float Tura;
 Soft_I2C_Init();
 Soft_I2C_Start();
 Soft_I2C_Write(0x90);
 Soft_I2C_Write(0xAA);
 Soft_I2C_Start();
 Soft_I2C_Write(0x91);
 DataxH = Soft_I2C_Read(1);
 DataxL = Soft_I2C_Read(0);
 Soft_I2C_Stop();
 tura = DataxH;
 if (DataxL == 128) tura = tura + 0.5;
 return(tura);
}
#line 1 "f:/projects/pic/temperature-monitoring-system/code/../code/components/real_clock.c"
void Write_RTC()
{
 Soft_I2C_Init();
 Soft_I2C_Start();
 Soft_I2C_Write(0xD0);
 Soft_I2C_Write(0);
 Soft_I2C_Write(0);
 Soft_I2C_Write(Ms);
 Soft_I2C_Write(HS);
 Soft_I2C_Write(1);
 Soft_I2C_Write(DD);
 Soft_I2C_Write(MM);
 Soft_I2C_Write(AA);
 Soft_I2C_Stop();
 Delay_ms(500);
}


void Read_RTC(){
 Soft_I2C_Init();
 Soft_I2C_Start();
 Soft_I2C_Write(0xD0);
 Soft_I2C_Write(0);
 Soft_I2C_Start();
 Soft_I2C_Write(0xD1);
 Segs= bcd2dec(Soft_I2C_Read(1));
 Ms = bcd2dec(Soft_I2C_Read(1));
 Hs = bcd2dec(Soft_I2C_Read(1));
 Date= bcd2dec(Soft_I2C_Read(1));
 DD = bcd2dec(Soft_I2C_Read(1));
 MM = bcd2dec(Soft_I2C_Read(1));
 AA = bcd2dec(Soft_I2C_Read(0));
 Soft_I2C_Stop();
}
#line 12 "F:/Projects/PIC/temperature-monitoring-system/code/TemperatureMonitoringSystem.c"
void main(){
 set_bits_for_LCD();

 Lcd_Init();

 Lcd_show_copyrigth();

 Helper_config_components();

 while(1) {

 }
}
