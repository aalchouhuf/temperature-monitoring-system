
_Move_Delay:

;lcddisplay.c,24 :: 		
;lcddisplay.c,25 :: 		
	MOVLW      3
	MOVWF      R11+0
	MOVLW      138
	MOVWF      R12+0
	MOVLW      85
	MOVWF      R13+0
L_Move_Delay0:
	DECFSZ     R13+0, 1
	GOTO       L_Move_Delay0
	DECFSZ     R12+0, 1
	GOTO       L_Move_Delay0
	DECFSZ     R11+0, 1
	GOTO       L_Move_Delay0
	NOP
	NOP
;lcddisplay.c,26 :: 		
L_end_Move_Delay:
	RETURN
; end of _Move_Delay

_set_bits_for_LCD:

;lcddisplay.c,28 :: 		
;lcddisplay.c,29 :: 		
	CLRF       ANSEL+0
;lcddisplay.c,30 :: 		
	CLRF       ANSELH+0
;lcddisplay.c,31 :: 		
	BCF        C1ON_bit+0, BitPos(C1ON_bit+0)
;lcddisplay.c,32 :: 		
	BCF        C2ON_bit+0, BitPos(C2ON_bit+0)
;lcddisplay.c,33 :: 		
L_end_set_bits_for_LCD:
	RETURN
; end of _set_bits_for_LCD

_Lcd_wait_500:

;lcddisplay.c,35 :: 		
;lcddisplay.c,36 :: 		
	MOVLW      3
	MOVWF      R11+0
	MOVLW      138
	MOVWF      R12+0
	MOVLW      85
	MOVWF      R13+0
L_Lcd_wait_5001:
	DECFSZ     R13+0, 1
	GOTO       L_Lcd_wait_5001
	DECFSZ     R12+0, 1
	GOTO       L_Lcd_wait_5001
	DECFSZ     R11+0, 1
	GOTO       L_Lcd_wait_5001
	NOP
	NOP
;lcddisplay.c,37 :: 		
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;lcddisplay.c,38 :: 		
L_end_Lcd_wait_500:
	RETURN
; end of _Lcd_wait_500

_Lcd_write_two_lines:

;lcddisplay.c,40 :: 		
;lcddisplay.c,41 :: 		
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVF       FARG_Lcd_write_two_lines_text_row_1+0, 0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;lcddisplay.c,42 :: 		
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVF       FARG_Lcd_write_two_lines_text_row_2+0, 0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;lcddisplay.c,43 :: 		
L_end_Lcd_write_two_lines:
	RETURN
; end of _Lcd_write_two_lines

_Lcd_show_copyrigth:

;lcddisplay.c,45 :: 		
;lcddisplay.c,46 :: 		
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;lcddisplay.c,47 :: 		
	MOVLW      12
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;lcddisplay.c,48 :: 		
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      6
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      _copyrigth+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;lcddisplay.c,49 :: 		
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      6
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      _reserved_to+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;lcddisplay.c,50 :: 		
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_Lcd_show_copyrigth2:
	DECFSZ     R13+0, 1
	GOTO       L_Lcd_show_copyrigth2
	DECFSZ     R12+0, 1
	GOTO       L_Lcd_show_copyrigth2
	DECFSZ     R11+0, 1
	GOTO       L_Lcd_show_copyrigth2
	NOP
	NOP
;lcddisplay.c,52 :: 		
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;lcddisplay.c,54 :: 		
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      _memb1_name+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;lcddisplay.c,55 :: 		
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      5
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      _memb1_CI+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;lcddisplay.c,56 :: 		
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_Lcd_show_copyrigth3:
	DECFSZ     R13+0, 1
	GOTO       L_Lcd_show_copyrigth3
	DECFSZ     R12+0, 1
	GOTO       L_Lcd_show_copyrigth3
	DECFSZ     R11+0, 1
	GOTO       L_Lcd_show_copyrigth3
	NOP
	NOP
;lcddisplay.c,58 :: 		
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;lcddisplay.c,60 :: 		
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      _memb2_name+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;lcddisplay.c,61 :: 		
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      5
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      _memb2_CI+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;lcddisplay.c,62 :: 		
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_Lcd_show_copyrigth4:
	DECFSZ     R13+0, 1
	GOTO       L_Lcd_show_copyrigth4
	DECFSZ     R12+0, 1
	GOTO       L_Lcd_show_copyrigth4
	DECFSZ     R11+0, 1
	GOTO       L_Lcd_show_copyrigth4
	NOP
	NOP
;lcddisplay.c,64 :: 		
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;lcddisplay.c,66 :: 		
	MOVLW      _memb1_name+0
	MOVWF      FARG_Lcd_write_two_lines_text_row_1+0
	MOVLW      _memb2_name+0
	MOVWF      FARG_Lcd_write_two_lines_text_row_2+0
	CALL       _Lcd_write_two_lines+0
;lcddisplay.c,67 :: 		
L_end_Lcd_show_copyrigth:
	RETURN
; end of _Lcd_show_copyrigth

_Mode_State:

;check_state.c,13 :: 		
;check_state.c,15 :: 		
L_Mode_State5:
;check_state.c,16 :: 		
	BTFSC      RD3_bit+0, BitPos(RD3_bit+0)
	GOTO       L_Mode_State8
;check_state.c,18 :: 		
	MOVLW      33
	MOVWF      R12+0
	MOVLW      118
	MOVWF      R13+0
L_Mode_State9:
	DECFSZ     R13+0, 1
	GOTO       L_Mode_State9
	DECFSZ     R12+0, 1
	GOTO       L_Mode_State9
	NOP
;check_state.c,19 :: 		
	BTFSS      RD3_bit+0, BitPos(RD3_bit+0)
	GOTO       L_Mode_State10
;check_state.c,20 :: 		
	INCF       _e+0, 1
	BTFSC      STATUS+0, 2
	INCF       _e+1, 1
;check_state.c,21 :: 		
L_Mode_State10:
;check_state.c,22 :: 		
L_Mode_State8:
;check_state.c,23 :: 		
	MOVLW      0
	XORWF      _e+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__Mode_State31
	MOVLW      1
	XORWF      _e+0, 0
L__Mode_State31:
	BTFSS      STATUS+0, 2
	GOTO       L_Mode_State11
	BSF        RA1_bit+0, BitPos(RA1_bit+0)
	BCF        RA3_bit+0, BitPos(RA3_bit+0)
L_Mode_State11:
;check_state.c,24 :: 		
	MOVLW      0
	XORWF      _e+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__Mode_State32
	MOVLW      2
	XORWF      _e+0, 0
L__Mode_State32:
	BTFSS      STATUS+0, 2
	GOTO       L_Mode_State12
	BSF        RA2_bit+0, BitPos(RA2_bit+0)
	BCF        RA1_bit+0, BitPos(RA1_bit+0)
L_Mode_State12:
;check_state.c,25 :: 		
	MOVLW      0
	XORWF      _e+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__Mode_State33
	MOVLW      3
	XORWF      _e+0, 0
L__Mode_State33:
	BTFSS      STATUS+0, 2
	GOTO       L_Mode_State13
	BSF        RA3_bit+0, BitPos(RA3_bit+0)
	BCF        RA2_bit+0, BitPos(RA2_bit+0)
L_Mode_State13:
;check_state.c,26 :: 		
	MOVLW      128
	MOVWF      R0+0
	MOVLW      128
	XORWF      _e+1, 0
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__Mode_State34
	MOVF       _e+0, 0
	SUBLW      3
L__Mode_State34:
	BTFSC      STATUS+0, 0
	GOTO       L_Mode_State14
	CLRF       _e+0
	CLRF       _e+1
	BCF        RA1_bit+0, BitPos(RA1_bit+0)
	BCF        RA2_bit+0, BitPos(RA2_bit+0)
	BCF        RA3_bit+0, BitPos(RA3_bit+0)
L_Mode_State14:
;check_state.c,27 :: 		
	BTFSC      RD2_bit+0, BitPos(RD2_bit+0)
	GOTO       L_Mode_State5
;check_state.c,28 :: 		
L_end_Mode_State:
	RETURN
; end of _Mode_State

_Write_EEPROM:

;eprom_memory.c,1 :: 		
;eprom_memory.c,3 :: 		
	CALL       _Soft_I2C_Init+0
;eprom_memory.c,4 :: 		
	CALL       _Soft_I2C_Start+0
;eprom_memory.c,5 :: 		
	MOVLW      160
	MOVWF      FARG_Soft_I2C_Write_data_+0
	CALL       _Soft_I2C_Write+0
;eprom_memory.c,6 :: 		
	MOVF       FARG_Write_EEPROM_Addres+1, 0
	MOVWF      R0+0
	MOVF       FARG_Write_EEPROM_Addres+2, 0
	MOVWF      R0+1
	MOVF       FARG_Write_EEPROM_Addres+3, 0
	MOVWF      R0+2
	MOVLW      0
	BTFSC      FARG_Write_EEPROM_Addres+3, 7
	MOVLW      255
	MOVWF      R0+3
	MOVF       R0+0, 0
	MOVWF      FARG_Soft_I2C_Write_data_+0
	CALL       _Soft_I2C_Write+0
;eprom_memory.c,7 :: 		
	MOVF       FARG_Write_EEPROM_Addres+0, 0
	MOVWF      FARG_Soft_I2C_Write_data_+0
	CALL       _Soft_I2C_Write+0
;eprom_memory.c,8 :: 		
	MOVF       FARG_Write_EEPROM_Data+0, 0
	MOVWF      FARG_Soft_I2C_Write_data_+0
	CALL       _Soft_I2C_Write+0
;eprom_memory.c,9 :: 		
	CALL       _Soft_I2C_Stop+0
;eprom_memory.c,10 :: 		
	MOVLW      2
	MOVWF      R11+0
	MOVLW      4
	MOVWF      R12+0
	MOVLW      186
	MOVWF      R13+0
L_Write_EEPROM15:
	DECFSZ     R13+0, 1
	GOTO       L_Write_EEPROM15
	DECFSZ     R12+0, 1
	GOTO       L_Write_EEPROM15
	DECFSZ     R11+0, 1
	GOTO       L_Write_EEPROM15
	NOP
;eprom_memory.c,11 :: 		
L_end_Write_EEPROM:
	RETURN
; end of _Write_EEPROM

_Read_EEPROM:

;eprom_memory.c,13 :: 		
;eprom_memory.c,15 :: 		
	CALL       _Soft_I2C_Init+0
;eprom_memory.c,16 :: 		
	CALL       _Soft_I2C_Start+0
;eprom_memory.c,17 :: 		
	MOVLW      160
	MOVWF      FARG_Soft_I2C_Write_data_+0
	CALL       _Soft_I2C_Write+0
;eprom_memory.c,18 :: 		
	MOVF       FARG_Read_EEPROM_Address+1, 0
	MOVWF      R0+0
	MOVF       FARG_Read_EEPROM_Address+2, 0
	MOVWF      R0+1
	MOVF       FARG_Read_EEPROM_Address+3, 0
	MOVWF      R0+2
	MOVLW      0
	BTFSC      FARG_Read_EEPROM_Address+3, 7
	MOVLW      255
	MOVWF      R0+3
	MOVF       R0+0, 0
	MOVWF      FARG_Soft_I2C_Write_data_+0
	CALL       _Soft_I2C_Write+0
;eprom_memory.c,19 :: 		
	MOVF       FARG_Read_EEPROM_Address+0, 0
	MOVWF      FARG_Soft_I2C_Write_data_+0
	CALL       _Soft_I2C_Write+0
;eprom_memory.c,20 :: 		
	CALL       _Soft_I2C_Start+0
;eprom_memory.c,21 :: 		
	MOVLW      161
	MOVWF      FARG_Soft_I2C_Write_data_+0
	CALL       _Soft_I2C_Write+0
;eprom_memory.c,22 :: 		
	CLRF       FARG_Soft_I2C_Read_ack+0
	CLRF       FARG_Soft_I2C_Read_ack+1
	CALL       _Soft_I2C_Read+0
	MOVF       R0+0, 0
	MOVWF      Read_EEPROM_Data_L0+0
	CLRF       Read_EEPROM_Data_L0+1
;eprom_memory.c,23 :: 		
	CALL       _Soft_I2C_Stop+0
;eprom_memory.c,24 :: 		
	MOVF       Read_EEPROM_Data_L0+0, 0
	MOVWF      R0+0
	MOVF       Read_EEPROM_Data_L0+1, 0
	MOVWF      R0+1
;eprom_memory.c,25 :: 		
L_end_Read_EEPROM:
	RETURN
; end of _Read_EEPROM

_Write_Float_EEPROM:

;eprom_memory.c,27 :: 		
;eprom_memory.c,29 :: 		
	CLRF       Write_Float_EEPROM_i_L0+0
	CLRF       Write_Float_EEPROM_i_L0+1
L_Write_Float_EEPROM16:
	MOVLW      128
	XORWF      Write_Float_EEPROM_i_L0+1, 0
	MOVWF      R0+0
	MOVLW      128
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__Write_Float_EEPROM38
	MOVLW      4
	SUBWF      Write_Float_EEPROM_i_L0+0, 0
L__Write_Float_EEPROM38:
	BTFSC      STATUS+0, 0
	GOTO       L_Write_Float_EEPROM17
;eprom_memory.c,30 :: 		
	MOVF       Write_Float_EEPROM_i_L0+0, 0
	MOVWF      FARG_Write_EEPROM_Addres+0
	MOVF       Write_Float_EEPROM_i_L0+1, 0
	MOVWF      FARG_Write_EEPROM_Addres+1
	MOVLW      0
	BTFSC      FARG_Write_EEPROM_Addres+1, 7
	MOVLW      255
	MOVWF      FARG_Write_EEPROM_Addres+2
	MOVWF      FARG_Write_EEPROM_Addres+3
	MOVF       FARG_Write_Float_EEPROM_n+0, 0
	ADDWF      FARG_Write_EEPROM_Addres+0, 1
	MOVF       FARG_Write_Float_EEPROM_n+1, 0
	BTFSC      STATUS+0, 0
	INCFSZ     FARG_Write_Float_EEPROM_n+1, 0
	ADDWF      FARG_Write_EEPROM_Addres+1, 1
	MOVF       FARG_Write_Float_EEPROM_n+2, 0
	BTFSC      STATUS+0, 0
	INCFSZ     FARG_Write_Float_EEPROM_n+2, 0
	ADDWF      FARG_Write_EEPROM_Addres+2, 1
	MOVF       FARG_Write_Float_EEPROM_n+3, 0
	BTFSC      STATUS+0, 0
	INCFSZ     FARG_Write_Float_EEPROM_n+3, 0
	ADDWF      FARG_Write_EEPROM_Addres+3, 1
	MOVF       Write_Float_EEPROM_i_L0+0, 0
	ADDLW      FARG_Write_Float_EEPROM_Data+0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      FARG_Write_EEPROM_Data+0
	CALL       _Write_EEPROM+0
;eprom_memory.c,29 :: 		
	INCF       Write_Float_EEPROM_i_L0+0, 1
	BTFSC      STATUS+0, 2
	INCF       Write_Float_EEPROM_i_L0+1, 1
;eprom_memory.c,30 :: 		
	GOTO       L_Write_Float_EEPROM16
L_Write_Float_EEPROM17:
;eprom_memory.c,31 :: 		
L_end_Write_Float_EEPROM:
	RETURN
; end of _Write_Float_EEPROM

_Read_Float_EEPROM:

;eprom_memory.c,33 :: 		
;eprom_memory.c,36 :: 		
	CLRF       Read_Float_EEPROM_i_L0+0
	CLRF       Read_Float_EEPROM_i_L0+1
L_Read_Float_EEPROM19:
	MOVLW      128
	XORWF      Read_Float_EEPROM_i_L0+1, 0
	MOVWF      R0+0
	MOVLW      128
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__Read_Float_EEPROM40
	MOVLW      4
	SUBWF      Read_Float_EEPROM_i_L0+0, 0
L__Read_Float_EEPROM40:
	BTFSC      STATUS+0, 0
	GOTO       L_Read_Float_EEPROM20
;eprom_memory.c,37 :: 		
	MOVF       Read_Float_EEPROM_i_L0+0, 0
	ADDLW      Read_Float_EEPROM_Data_L0+0
	MOVWF      FLOC__Read_Float_EEPROM+0
	MOVF       Read_Float_EEPROM_i_L0+0, 0
	MOVWF      FARG_Read_EEPROM_Address+0
	MOVF       Read_Float_EEPROM_i_L0+1, 0
	MOVWF      FARG_Read_EEPROM_Address+1
	MOVLW      0
	BTFSC      FARG_Read_EEPROM_Address+1, 7
	MOVLW      255
	MOVWF      FARG_Read_EEPROM_Address+2
	MOVWF      FARG_Read_EEPROM_Address+3
	MOVF       FARG_Read_Float_EEPROM_n+0, 0
	ADDWF      FARG_Read_EEPROM_Address+0, 1
	MOVF       FARG_Read_Float_EEPROM_n+1, 0
	BTFSC      STATUS+0, 0
	INCFSZ     FARG_Read_Float_EEPROM_n+1, 0
	ADDWF      FARG_Read_EEPROM_Address+1, 1
	MOVF       FARG_Read_Float_EEPROM_n+2, 0
	BTFSC      STATUS+0, 0
	INCFSZ     FARG_Read_Float_EEPROM_n+2, 0
	ADDWF      FARG_Read_EEPROM_Address+2, 1
	MOVF       FARG_Read_Float_EEPROM_n+3, 0
	BTFSC      STATUS+0, 0
	INCFSZ     FARG_Read_Float_EEPROM_n+3, 0
	ADDWF      FARG_Read_EEPROM_Address+3, 1
	CALL       _Read_EEPROM+0
	MOVF       FLOC__Read_Float_EEPROM+0, 0
	MOVWF      FSR
	MOVF       R0+0, 0
	MOVWF      INDF+0
;eprom_memory.c,36 :: 		
	INCF       Read_Float_EEPROM_i_L0+0, 1
	BTFSC      STATUS+0, 2
	INCF       Read_Float_EEPROM_i_L0+1, 1
;eprom_memory.c,37 :: 		
	GOTO       L_Read_Float_EEPROM19
L_Read_Float_EEPROM20:
;eprom_memory.c,38 :: 		
	MOVF       Read_Float_EEPROM_Data_L0+0, 0
	MOVWF      R0+0
	MOVF       Read_Float_EEPROM_Data_L0+1, 0
	MOVWF      R0+1
	MOVF       Read_Float_EEPROM_Data_L0+2, 0
	MOVWF      R0+2
	MOVF       Read_Float_EEPROM_Data_L0+3, 0
	MOVWF      R0+3
;eprom_memory.c,39 :: 		
L_end_Read_Float_EEPROM:
	RETURN
; end of _Read_Float_EEPROM

_Write_Temp_init:

;temperature_sensor.c,1 :: 		
;temperature_sensor.c,2 :: 		
	CALL       _Soft_I2C_Init+0
;temperature_sensor.c,3 :: 		
	CALL       _Soft_I2C_Start+0
;temperature_sensor.c,4 :: 		
	MOVLW      144
	MOVWF      FARG_Soft_I2C_Write_data_+0
	CALL       _Soft_I2C_Write+0
;temperature_sensor.c,5 :: 		
	MOVLW      238
	MOVWF      FARG_Soft_I2C_Write_data_+0
	CALL       _Soft_I2C_Write+0
;temperature_sensor.c,6 :: 		
	CALL       _Soft_I2C_Stop+0
;temperature_sensor.c,7 :: 		
L_end_Write_Temp_init:
	RETURN
; end of _Write_Temp_init

_Read_Full_Temp:

;temperature_sensor.c,9 :: 		
;temperature_sensor.c,13 :: 		
	CALL       _Soft_I2C_Init+0
;temperature_sensor.c,14 :: 		
	CALL       _Soft_I2C_Start+0
;temperature_sensor.c,15 :: 		
	MOVLW      144
	MOVWF      FARG_Soft_I2C_Write_data_+0
	CALL       _Soft_I2C_Write+0
;temperature_sensor.c,16 :: 		
	MOVLW      170
	MOVWF      FARG_Soft_I2C_Write_data_+0
	CALL       _Soft_I2C_Write+0
;temperature_sensor.c,17 :: 		
	CALL       _Soft_I2C_Start+0
;temperature_sensor.c,18 :: 		
	MOVLW      145
	MOVWF      FARG_Soft_I2C_Write_data_+0
	CALL       _Soft_I2C_Write+0
;temperature_sensor.c,19 :: 		
	MOVLW      1
	MOVWF      FARG_Soft_I2C_Read_ack+0
	MOVLW      0
	MOVWF      FARG_Soft_I2C_Read_ack+1
	CALL       _Soft_I2C_Read+0
	MOVF       R0+0, 0
	MOVWF      Read_Full_Temp_DataxH_L0+0
	CLRF       Read_Full_Temp_DataxH_L0+1
;temperature_sensor.c,20 :: 		
	CLRF       FARG_Soft_I2C_Read_ack+0
	CLRF       FARG_Soft_I2C_Read_ack+1
	CALL       _Soft_I2C_Read+0
	MOVF       R0+0, 0
	MOVWF      Read_Full_Temp_DataxL_L0+0
	CLRF       Read_Full_Temp_DataxL_L0+1
;temperature_sensor.c,21 :: 		
	CALL       _Soft_I2C_Stop+0
;temperature_sensor.c,22 :: 		
	MOVF       Read_Full_Temp_DataxH_L0+0, 0
	MOVWF      R0+0
	MOVF       Read_Full_Temp_DataxH_L0+1, 0
	MOVWF      R0+1
	CALL       _int2double+0
	MOVF       R0+0, 0
	MOVWF      Read_Full_Temp_Tura_L0+0
	MOVF       R0+1, 0
	MOVWF      Read_Full_Temp_Tura_L0+1
	MOVF       R0+2, 0
	MOVWF      Read_Full_Temp_Tura_L0+2
	MOVF       R0+3, 0
	MOVWF      Read_Full_Temp_Tura_L0+3
;temperature_sensor.c,23 :: 		
	MOVLW      0
	XORWF      Read_Full_Temp_DataxL_L0+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__Read_Full_Temp43
	MOVLW      128
	XORWF      Read_Full_Temp_DataxL_L0+0, 0
L__Read_Full_Temp43:
	BTFSS      STATUS+0, 2
	GOTO       L_Read_Full_Temp22
	MOVF       Read_Full_Temp_Tura_L0+0, 0
	MOVWF      R0+0
	MOVF       Read_Full_Temp_Tura_L0+1, 0
	MOVWF      R0+1
	MOVF       Read_Full_Temp_Tura_L0+2, 0
	MOVWF      R0+2
	MOVF       Read_Full_Temp_Tura_L0+3, 0
	MOVWF      R0+3
	MOVLW      0
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	MOVLW      0
	MOVWF      R4+2
	MOVLW      126
	MOVWF      R4+3
	CALL       _Add_32x32_FP+0
	MOVF       R0+0, 0
	MOVWF      Read_Full_Temp_Tura_L0+0
	MOVF       R0+1, 0
	MOVWF      Read_Full_Temp_Tura_L0+1
	MOVF       R0+2, 0
	MOVWF      Read_Full_Temp_Tura_L0+2
	MOVF       R0+3, 0
	MOVWF      Read_Full_Temp_Tura_L0+3
L_Read_Full_Temp22:
;temperature_sensor.c,24 :: 		
	MOVF       Read_Full_Temp_Tura_L0+0, 0
	MOVWF      R0+0
	MOVF       Read_Full_Temp_Tura_L0+1, 0
	MOVWF      R0+1
	MOVF       Read_Full_Temp_Tura_L0+2, 0
	MOVWF      R0+2
	MOVF       Read_Full_Temp_Tura_L0+3, 0
	MOVWF      R0+3
;temperature_sensor.c,25 :: 		
L_end_Read_Full_Temp:
	RETURN
; end of _Read_Full_Temp

_Helper_config_components:

;helper.c,1 :: 		
;helper.c,3 :: 		
	CLRF       TRISA+0
;helper.c,4 :: 		
	MOVLW      255
	MOVWF      TRISD+0
;helper.c,6 :: 		
	CLRF       PORTA+0
;helper.c,7 :: 		
	CLRF       ANSEL+0
;helper.c,8 :: 		
	CLRF       ANSELH+0
;helper.c,10 :: 		
	CALL       _Lcd_Init+0
;helper.c,11 :: 		
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;helper.c,12 :: 		
	MOVLW      12
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;helper.c,13 :: 		
L_end_Helper_config_components:
	RETURN
; end of _Helper_config_components

_main:

;TemperatureMonitoringSystem.c,12 :: 		void main(){
;TemperatureMonitoringSystem.c,13 :: 		set_bits_for_LCD();
	CALL       _set_bits_for_LCD+0
;TemperatureMonitoringSystem.c,15 :: 		Lcd_Init();                        // Initialize LCD
	CALL       _Lcd_Init+0
;TemperatureMonitoringSystem.c,17 :: 		Lcd_show_copyrigth();
	CALL       _Lcd_show_copyrigth+0
;TemperatureMonitoringSystem.c,19 :: 		Helper_config_components();
	CALL       _Helper_config_components+0
;TemperatureMonitoringSystem.c,21 :: 		while(1) {                         // Endless loop
L_main23:
;TemperatureMonitoringSystem.c,23 :: 		}
	GOTO       L_main23
;TemperatureMonitoringSystem.c,24 :: 		}
L_end_main:
	GOTO       $+0
; end of _main
