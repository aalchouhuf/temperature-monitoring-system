# Temperature Monitoring System

Design of a basic temperature monitoring system using PIC16F887 to be installed on a meat truck.

## Instrucciones en español

Diseno de un sistema basico de monitoreo de temperatura usando PIC16F887 para ser instalado en una camion de carga de carne.

Requerimientos:

- Sistema de monitoreo de tempreratura.
- La carga se traslada de A a B. (Dist Aprox ~ 100km)
- Velocidad Maxima Vmax = 60 km/h
- Temperatura Maxima de la carga Tmax < 20 .C
- Muestreo de temperatura a una frecuencia de 10Hz (10 Samples/s )
- Tiempo de Carga (Tcarg ~ 10 min)
- Tiempo de Descarga (Tdescarg ~ 10 min)
- Monitoria la temperatura durante Tcarg y Tdescarg
- Velocidad media (Vmed = 30 km/h)
- Datos separados por comas (" , ")
- Generar interrupcion al hacer la captura de temperatura (Sin pooling o modo encuesta)
- El modo visualizacion de data debe mostrar durante un segundo cada lectura con su debido timestamp durante un segundo, y de forma secuencial hasta mostrar todos los datos.

## Modos de Operacion

- Apagado.
- Estado Encendido: Iniciar configuracion.
- Estado Encendido: Monitoreo.
- Estado Encendido: Mostrar Data.
- Estado Encendido: Transmitir Data a PC. (No implementado)

El sistema debe ser completamente autonomo, y almacenar la data del viaje en una memoria EPROM, con marcas de tiempo HORA-FECHA por muestra.

## Hardware Implementado

- PIC16F887
- 

# English instructions

Design of a basic temperature monitoring system using PIC16F887 to be installed on a meat truck.

Requirements:

- Temprerature monitoring system.
- The load is moved from A to B. (Approx Dist ~ 100km)
- Maximum speed Vmax = 60 km / h
- Maximum temperature of the load Tmax < 20 .C
- Temperature sampling at a frequency of 10Hz (10 Samples / s)
- Charging Time (Tload ~ 10 min)
- Download Time (Tdownload ~ 10 min)
- Monitors the temperature during Tload and Tdownload
- Average speed (Vmed = 30 km / h)
- Data separated by commas (",")
- Generate interrupt when capturing temperature (Without pooling or survey mode)
- The data display mode must show each reading for one second with its proper timestamp for one second, and sequentially until all data is displayed.

## Operating Modes

- Off.
- On Status: Start configuration.
- On Status: Monitoring.
- Status On: Show Data.
- On Status: Transmit Data to PC. (Not implemented)

The system must be completely autonomous, and store the trip data in an EPROM memory, with TIME-DATE timestamps per sample.

## Implemented Hardware

- PIC16F887
-
